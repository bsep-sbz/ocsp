import requests
import base64
from io import BytesIO

import pem
from cryptography.x509 import load_pem_x509_certificate
from cryptography.x509.ocsp import (
    OCSPRequestBuilder,
    load_der_ocsp_response
)
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.hashes import SHA1


def send():
    pem_cert = str(pem.parse_file('/home/alex/tmp/Novi Sad Office Unit 2.cert')[0])
    cert_stream = BytesIO(bytes(pem_cert, encoding='utf-8'))
    cert = load_pem_x509_certificate(cert_stream.read(), default_backend())

    issuer_pem_cert = str(pem.parse_file('../secrets/issuers-bundle.pem')[0])
    cert_stream = BytesIO(bytes(issuer_pem_cert, encoding='utf-8'))
    issuer_cert = load_pem_x509_certificate(cert_stream.read(), default_backend())

    builder = OCSPRequestBuilder()
    req = builder.add_certificate(cert, issuer_cert, SHA1()).build()

    data = base64.urlsafe_b64encode(req.public_bytes(serialization.Encoding.DER))
    data = data.decode('utf-8')

    url = 'https://ocsp.megatravel.com/certificates/'
    response = requests.get(url + data)

    ocsp_resp = load_der_ocsp_response(response.content)
    print(ocsp_resp.response_status)
    print(ocsp_resp.certificate_status)

    response = requests.post(
        url='https://ocsp.megatravel.com/certificates',
        data=req.public_bytes(serialization.Encoding.DER),
        headers={
            'Content-Type': 'application/ocsp-request'
        },
    )

    ocsp_resp = load_der_ocsp_response(response.content)
    print(ocsp_resp.response_status)
    print(ocsp_resp.certificate_status)


if __name__ == '__main__':
    send()
