import base64
from io import BytesIO

from flask import (

    jsonify,
    send_file,
    request,
    Blueprint
)

from pki.ocsp.services import (
    make_ocsp_response,
    register_certificate,
    revoke_certificate
)
from pki.ocsp.utils import (
    logged,
    requires_issuer
)

router = Blueprint('router', __name__)


@router.route('/admin/certificates/register', methods=['POST'])
@logged
@requires_issuer
def register_cert(**kwargs):
    log = kwargs['log']
    issuer = kwargs['issuer']

    sn = register_certificate(request.get_data(), issuer)

    log['msg'] = 'Successful certificate registration'
    log['registeredCertSer'] = int(sn)

    return 'OK'


@router.route('/admin/certificates/revoke', methods=['POST'])
@logged
@requires_issuer
def revoke_cert(**kwargs):
    log = kwargs['log']
    issuer = kwargs['issuer']

    sn, reason, affected = revoke_certificate(request.get_data(), issuer)

    log['msg'] = 'Successful certificate revocation'
    log['revokedCertSer'] = int(sn)
    log['revocationReason'] = reason
    log['affectedCerts'] = affected

    return 'OK'


@router.route('/certificates', methods=['POST'], defaults={'data': None})
@router.route('/certificates/<string:data>', methods=['GET'])
@logged
def cert_status(data, **kwargs):
    log = kwargs['log']
    log['msg'] = 'Certificate status request'

    if request.method == 'POST':
        req = request.get_data()
    else:
        req = base64.urlsafe_b64decode(data)
    resp = make_ocsp_response(req)
    return send_file(BytesIO(resp), mimetype='application/ocsp-response')


@router.route('/health')
def health():
    return jsonify({'status': 'OK'})
