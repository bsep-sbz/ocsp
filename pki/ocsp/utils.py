import json
import logging
import urllib.parse
from functools import wraps
from io import BytesIO

import jwt

from cryptography.hazmat.backends import default_backend
from cryptography.x509 import (
    load_pem_x509_certificate,
    Certificate
)
from cryptography.x509.name import Name, NameOID
from flask import (
    current_app as app,
    request
)


class Error(RuntimeError):

    def __init__(self, code, msg, level):
        self.code = code
        self.level = level
        super().__init__(msg)


def digest_log(log):
    level = log['level']
    del log['level']
    app.logger.log(level, json.dumps(log, indent=2, sort_keys=True))


def logged(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        ip = request.headers.get('X-Forwarded-For', None) or \
             request.remote_addr
        log = {
            'level': logging.INFO,
            'url': request.url,
            'requesterIp': ip,
            'method': request.method
        }
        kwargs['log'] = log

        try:
            res = func(*args, **kwargs)
            log['status'] = 'success'
            digest_log(log)
            return res

        except Error as e:
            log['msg'] = str(e)
            log['level'] = e.level
            log['status'] = 'fail'
            digest_log(log)
            return "", e.code

    return wrapper


def requires_issuer(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        log = kwargs['log']
        if 'X-SSL-CERT' not in request.headers:
            log['msg'] = 'Certificate not provided'
            log['level'] = logging.WARNING
            return '', 403

        kwargs['issuer'] = extract_issuer(log)

        try:
            return func(*args, **kwargs)

        except jwt.exceptions.InvalidSignatureError:
            raise Error(403, 'Invalid signature', logging.WARNING)
        except jwt.exceptions.InvalidTokenError:
            raise Error(400, 'Invalid token', logging.INFO)
        except RuntimeError as e:
            raise Error(400, str(e), logging.INFO)

    return wrapper


def extract_issuer(log):
    issuer_pem = urllib.parse.unquote(request.headers['X-SSL-CERT'])
    issuer_bytes = BytesIO(bytes(issuer_pem, encoding='utf-8'))
    issuer: Certificate = load_pem_x509_certificate(
        data=issuer_bytes.read(),
        backend=default_backend()
    )

    subject: Name = issuer.subject
    log['requester'] = \
        subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value
    log['requesterSer'] = issuer.serial_number

    return issuer
