import os


DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

KEY_PATH = os.environ.get('KEY_PATH', None)
CERT_PATH = os.environ.get('CERT_PATH', None)
ISSUERS_PATH = os.environ.get('ISSUERS_PATH', None)

DB_ADDRESS = os.environ.get('DB_ADDRESS', '')
DB_PORT = os.environ.get('DB_PORT', '')
DB_NAME = os.environ.get('DB_NAME', '')
DB_USER = os.environ.get('DB_USER', '')
DB_PASSWORD = os.environ.get('DB_PASSWORD', '')

DB_DRIVER = 'postgresql+psycopg2'
DB_OPTIONS = '?use_batch_mode&client_encoding=utf8'
SQLALCHEMY_DATABASE_URI = f'{DB_DRIVER}://{DB_USER}:{DB_PASSWORD}@{DB_ADDRESS}:{DB_PORT}/{DB_NAME}{DB_OPTIONS}'
