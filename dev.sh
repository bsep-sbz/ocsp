#!/usr/bin/env bash
source venv/bin/activate
cd ./pki/ocsp
flask db init
flask db migrate
flask db upgrade
