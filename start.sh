#!/usr/bin/env bash
while ! nc -z db 5432; do sleep 3; done

cd pki/ocsp

flask db upgrade
gunicorn -b 0.0.0.0:8080 app
